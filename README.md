# cloudlightning-gateway-service

The *CloudLightning Gateway Service* represents the entry point to the CloudLightning ecosystem. It is based on [Alien4Cloud](https://github.com/alien4cloud/alien4cloud) and [Brooklyn TOSCA](https://github.com/cloudsoft/brooklyn-tosca).

## Instalation
In order to download all submodules, you need to run the following two commands:
```
git submodule init
git submodule update
```
This will ensure you are ready to install the following components:
### 1 gateway-dependency-a4c
This module contains the alien4cloud base repository, needed to compile the CloudLightning user interface in the brooklyn-tosca module.
Switch directory to `gateway-dependency-a4c` and execute `mvn clean install`. 

### 2 cl-brooklyn-tosca
This module contains the brooklyn modifications to support TOSCA language, together with the CloudLightning plugin used to deploy the topologies using either Brooklyn or Marathon. 
After installing the gateway-dependency-a4c, you can run `mvn clean install assembly:single` in the root directory. 
To launch a standalone A4C instance, edit the config file in
`alien4cloud-standalone/` as desired then run:

    nohup alien4cloud-standalone/alien4cloud.sh &
    
Alien4Cloud will be running on port 8091, as set in that config.

To override the `brooklyn` launch to use an existing A4C installation,
set `client: false` and `hosts: <other-alien-es-backend-ip-port>` 
in the `conf/alien4cloud-config.yml` used by this launch
(or specify a different `alien4cloud-config.file` for brooklyn).
For example if you want to run a local Brooklyn against a local but separate A4C,
use:

    nohup ./brooklyn.sh launch -Dalien4cloud-config.file=conf/alien4cloud-config.client-to-localhost.yml &

Note that A4C launches ES with no credentials required, 
so the ES instance should be secured at the network level
(which they are in this configuration as it is only bound to localhost).

Any ElasticSearch data stored by this instance will use default ES file locations.
The recommended way to configure ES data is by launching a separate Alien4Cloud instance 
configured as desired, with this instance pointing at that.

### 2.1 CloudLightning Brooklyn Plugin
Change directory into `a4c-brooklyn-plugin/target` and drop `a4c-brooklyn-plugin-0.11.0-SNAPSHOT.zip` in the *Plugins* tab of the UI. 
![Plugin drag and drop](images/a4c-brooklyn.png "CloudLightning Orchestrator Plugin drag and drop")

### 3 data-model
This module contains classes needed for communication with the SOSM sistem. 
Switch directory to `cloudlightning-data-model` and execute `mvn clean install`.

### 4 gateway-sde
Switch direcory to `gateway-sde` and execute `mvn clean package spring-boot:repackage`.
Run `java -jar ./target/sde-rest-1.3.3.2.jar`. Make sure the Alien4Cloud UI has started before starting this component, as it requires authentication to the UI. 

### 5 a4c-input-export-plugin
This module contains the UI plugin needed to communicate with the Service Decomposition Engine in order to replace the abstract nodes with concrete implementations. 
Switch directory to `a4c-input-export-plugin` and execute `mvn clean package`.
Drop the plugin `target/cloudlightning-plugin-sample-1.3.3.2.zip` in the UI *Plugins* tab. 
![Plugin drag and drop](images/a4c-sde.png "CloudLightning Orchestrator Plugin drag and drop")

### 6 CloudLightning SDL
The CloudLightning Service Description Language extends OASIS [TOSCA](http://docs.oasis-open.org/tosca/TOSCA/v1.0/TOSCA-v1.0.html) and contains service definitions for the use cases presented in CloudLightning: RayTracing engine and WebUI, Oil & Gas Upscale engine, Genomics sequencing engine, and BLAS libraries. 
To make use of these definitions create a zip inside each of its subfolders (tosca-nomative-types, docker-types, cl-normative, etc) and upload them to the *Components* tab in the following order:
  1. tosca-normative-types-master 
  2. docker-types 
  3. cl-normative 
  4. The order of the use-case definitions does not matter, as they do not depend on eachother 

## Example Usage
To be updated. 
